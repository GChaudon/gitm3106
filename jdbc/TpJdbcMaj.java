import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class TpJdbcMaj {
	public static void mettreAJourNbh(Connection conn) throws SQLException {
		System.out.println("Mise à jour de la table: ");
		Statement stEns = conn.createStatement();
		stEns.executeQuery("update enseignant set nbhdisp = nbhdisp + 1.5 where idenseign = 'PGM'");
		System.out.println(stEns.getUpdateCount() + " ligne(s) mise(s) à jour.");
		stEns.close();
	}
}
