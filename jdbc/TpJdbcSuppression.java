import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class TpJdbcSuppression {
	public static void supprimerCreneaux11h(Connection conn) throws SQLException {
		Statement stCreneau = conn.createStatement();
		
		stCreneau.executeQuery("delete from enseigner where heuredc = '11:00'");
		System.out.println(stCreneau.getUpdateCount() + " lignes(s) supprimée(s) dans la table enseigner");
		stCreneau.executeQuery("delete from affecter where heuredc = '11:00'");
		System.out.println(stCreneau.getUpdateCount() + " lignes(s) supprimée(s) dans la table affecter");
		stCreneau.executeQuery("delete from creneau where heuredc = '11:00'");
		System.out.println(stCreneau.getUpdateCount() + " ligne(s) supprimée(s) dans la table créneau");
		
		stCreneau.close();
	}
}
