import java.sql.SQLException;

import oracle.jdbc.pool.OracleDataSource;

public class CictOracleDataSource extends OracleDataSource {
	public CictOracleDataSource() throws SQLException {
		super();
		this.setURL("jdbc:oracle:thin:@telline.univ-tlse3.fr:1521:etupre");
		this.setUser(Credentials.USERNAME);
		this.setPassword(Credentials.PASSWORD);
	}
	
	public static void main(String[] args) {
		try {
			CictOracleDataSource db = new CictOracleDataSource();	
			System.out.println("Connexion réussie !");
		} catch(SQLException e) {
			e.printStackTrace();
		}
		
	}
}
