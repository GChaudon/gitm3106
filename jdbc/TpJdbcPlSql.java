import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;

public class TpJdbcPlSql {
	public static void validerHeures(Connection conn) throws SQLException {
		CallableStatement stEns = conn.prepareCall("{call validerHeures(?, ?)}");
		stEns.setString(1, "ADM");
		stEns.setFloat(2, 40.5F);
		stEns.execute();
		
		System.out.println("Heures validées");
		
		stEns.close();
	}
	
	public static void nbHDispTotal(Connection conn) throws SQLException {
		CallableStatement stEns = conn.prepareCall("{? = call nbHDispTotal}");
		stEns.registerOutParameter(1, Types.FLOAT);
		stEns.execute();
		System.out.println("Nombre total d'heures dispensées : " + (int) stEns.getFloat(1) + " heures");
		stEns.close();
	}
}
