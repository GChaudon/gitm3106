import java.sql.Connection;
import java.sql.SQLException;

public class TpJdbc {
	public static void main(String[] args) throws SQLException {
		System.out.println("Hello, World");

		Connection conn = new CictOracleDataSource().getConnection();
		// Mauvais host: Unknown Host Exception
		// Mauvais username: SQLException: nom utilisateur / mot de passe invalide
		// Mauvais mdp: SQLException: nom utilisateur / mot de passe invalide
		System.out.println("Connection au SGBD établie");
		
		// TpJdbcInterrogation.afficherNbHeuresEnseignants(conn);
		
		// TpJdbcMaj.mettreAJourNbh(conn);
		
		// TpJdbcSuppression.supprimerCreneaux11h(conn);
		
		//TpJdbcInsertion.insererEnseignant(conn);
		
		// TpJdbcPlSql.validerHeures(conn);
		
		// TpJdbcPlSql.nbHDispTotal(conn);
		
		TpJdbcPlSqlCurseur.listerEnseignants(conn);
		
		conn.close();
	}
}
