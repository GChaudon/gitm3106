import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class TpJdbcInsertion {
	public static void insererEnseignant(Connection conn) throws SQLException {
		Statement stEns = conn.createStatement();
		stEns.executeQuery("insert into enseignant values('DRE', 'Young', 'Andre', 'PROF', 0)");
		System.out.println(stEns.getUpdateCount() + " ligne(s) insérée(s)");
		stEns.close();
	}
}
