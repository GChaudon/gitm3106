import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import oracle.jdbc.OracleTypes;

public class TpJdbcPlSqlCurseur {
	public static void listerEnseignants(Connection conn) throws SQLException {
		CallableStatement stEns = conn.prepareCall("{? = call listeEnseignants}");
		stEns.registerOutParameter(1, OracleTypes.CURSOR);
		stEns.execute();
		ResultSet rsEns = (ResultSet) stEns.getObject(1);
		while(rsEns.next()) {
			String nom = rsEns.getString("nom");
			String grade = rsEns.getString("grade");
			System.out.println(nom + " - " + grade);
		}
	}
}
