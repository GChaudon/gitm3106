import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JOptionPane;

public class TpJdbcSwing {
	public static void main(String[] args) {
		final String TITRE_FEN = "Gestion des heures d'enseignement";
		String msg;
		
		String strIdEns = JOptionPane.showInputDialog(null, "Quel est votre identifiant", TITRE_FEN, JOptionPane.QUESTION_MESSAGE);
		try {
			double nbHDisp = getNbHDisp(strIdEns);
			
			if(nbHDisp == 0) msg = "Vous n'avez pas encore dispensé d'heures d'enseignements.";
			else msg = "Vous avez dispensé "+nbHDisp+" heure"+(nbHDisp > 1 ? "s" : "")+" d'enseignement.";
			
		} catch(Exception e) {
			msg = e.getMessage();
		}
		
		JOptionPane.showMessageDialog(null,  msg, TITRE_FEN, JOptionPane.INFORMATION_MESSAGE);
	}
	
	public static double getNbHDisp(String idEnseign) throws Exception, SQLException {
		Connection cn = new CictOracleDataSource().getConnection();
		Statement st = cn.createStatement();
		String query = "select nbhdisp from enseignant where idenseign='"+idEnseign+"'";
		sysout
		ResultSet rs = st.executeQuery(query);
		if(!rs.next()) throw new Exception("Il n'y a aucun enseignant d'identifiant "+idEnseign);
		double nbHDisp = rs.getDouble(1);
		cn.close();
		return nbHDisp;
	}
}
