import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class TpJdbcInterrogation {
	public static void afficherNbHeuresEnseignants(Connection conn) throws SQLException {
		System.out.println("Affichage du nombre d'heures dispensées par enseignant");
		Statement stEns = conn.createStatement();
		ResultSet rsEns = stEns.executeQuery("select nom, nbh from nbhdispenseparenseignant n, enseignant e where n.ensc = e.idenseign");
		while(rsEns.next()) {
			String enseignant = rsEns.getString("nom");
			float nbH = rsEns.getFloat("nbh");
			System.out.println(enseignant + " a dispensé " + nbH + "h d'enseignement EQTD");
		}
	}
}
