create table GROUPE(
    GRPC varchar2(20),
    ANNEEC char(3)
);

create table CRENEAU(
    DEBSEMC date, 
    JOURC varchar2(8),
    HEUREDC char(5),
    TYPEC varchar2(20),
    HEUREFC char(5), 
    GRPC varchar2(20),
    MATC varchar2(10)
);

create table ENSEIGNER(
    DEBSEMC date,
    JOURC varchar2(8),
    HEUREDC char(5),
    GRPC varchar2(20),
    ENSC char(3)
);

create table AFFECTER(
    DEBSEMC date, 
    JOURC varchar2(8),
    HEUREDC char(5),
    GRPC varchar2(20),
    SALC varchar2(15)
);

alter table GROUPE add constraint PK_GROUPE primary key(GRPC);
alter table CRENEAU add constraint PK_CRENEAU primary key (DEBSEMC, JOURC, HEUREDC, GRPC);
alter table ENSEIGNER add constraint PK_ENSEIGNER primary key(DEBSEMC, JOURC, HEUREDC, GRPC, ENSC);
alter table AFFECTER add constraint PK_AFFECTER primary key(DEBSEMC, JOURC, HEUREDC, GRPC, SALC);
alter table CRENEAU add constraint FK_CRENEAU_MATC foreign key(MATC) references MAT(MATC);
alter table CRENEAU add constraint FK_CRENEAU_GRPC foreign key(GRPC) references GROUPE(GRPC);
alter table AFFECTER add constraint FK_AFFECTER_GRPC foreign key(GRPC) references GROUPE(GRPC);
alter table ENSEIGNER add constraint FK_ENSEIGNER_GRPC foreign key(GRPC) references GROUPE(GRPC);
alter table AFFECTER add constraint FK_AFFECTER_DEBSEMC_GRPC foreign key(DEBSEMC, JOURC, HEUREDC, GRPC) references CRENEAU(DEBSEMC, JOURC, HEUREDC, GRPC);
alter table ENSEIGNER add constraint FK_ENSEIGNER_DEBSEMC_GRPC foreign key(DEBSEMC, JOURC, HEUREDC, GRPC) references CRENEAU(DEBSEMC, JOURC, HEUREDC, GRPC);

delete from GROUPE;

insert into GROUPE(GRPC, ANNEEC)
select distinct GRPC, ANNEEC from thierry_millan.CELCAT2017 where grpc is not null;

delete from CRENEAU;

insert into CRENEAU(DEBSEMC, JOURC, HEUREDC, TYPEC, HEUREFC, GRPC, MATC)
select distinct DEBSEMC, JOURC, HEUREDC, TYPEC, HEUREFC, GRPC, MATC from thierry_millan.CELCAT2017;

delete from ENSEIGNER;

insert into ENSEIGNER(DEBSEMC, JOURC, HEUREDC, GRPC, ENSC)
select distinct DEBSEMC, JOURC, HEUREDC, GRPC, ENSC from thierry_millan.CELCAT2017 where ENSC is not null;

delete from AFFECTER;

insert into AFFECTER(DEBSEMC, JOURC, HEUREDC, GRPC, SALC)
select distinct DEBSEMC, JOURC, HEUREDC, GRPC, SALC from thierry_millan.CELCAT2017 where SALC is not null;

commit;

-- Verification de la cohérence des données

select CRENEAU.DEBSEMC, CRENEAU.JOURC, CRENEAU.HEUREDC, TYPEC, HEUREFC, CRENEAU.GRPC, CRENEAU.MATC, SALC, ENSC, ANNEEC, INTC
from CRENEAU, MAT, GROUPE, ENSEIGNER, AFFECTER
where CRENEAU.GRPC = GROUPE.GRPC
and MAT.MATC (+) = CRENEAU.MATC
and AFFECTER.DEBSEMC (+) = CRENEAU.DEBSEMC
and AFFECTER.JOURC (+) = CRENEAU.JOURC
and AFFECTER.HEUREDC (+) = CRENEAU.HEUREDC
and AFFECTER.GRPC (+) = CRENEAU.GRPC
and ENSEIGNER.DEBSEMC (+) = CRENEAU.DEBSEMC
and ENSEIGNER.JOURC (+) = CRENEAU.JOURC
and ENSEIGNER.HEUREDC (+) = CRENEAU.HEUREDC
and ENSEIGNER.GRPC (+) = CRENEAU.GRPC
minus
select DEBSEMC, JOURC, HEUREDC, TYPEC, HEUREFC, GRPC, MATC, SALC, ENSC, ANNEEC, INTC
from thierry_millan.CELCAT2017;

select DEBSEMC, JOURC, HEUREDC, TYPEC, HEUREFC, GRPC, MATC, SALC, ENSC, ANNEEC, INTC
from thierry_millan.CELCAT2017
minus
select CRENEAU.DEBSEMC, CRENEAU.JOURC, CRENEAU.HEUREDC, TYPEC, HEUREFC, CRENEAU.GRPC, CRENEAU.MATC, SALC, ENSC, ANNEEC, INTC
from CRENEAU, MAT, GROUPE, ENSEIGNER, AFFECTER
where CRENEAU.GRPC = GROUPE.GRPC
and MAT.MATC (+) = CRENEAU.MATC
and AFFECTER.DEBSEMC (+) = CRENEAU.DEBSEMC
and AFFECTER.JOURC (+) = CRENEAU.JOURC
and AFFECTER.HEUREDC (+) = CRENEAU.HEUREDC
and AFFECTER.GRPC (+) = CRENEAU.GRPC
and ENSEIGNER.DEBSEMC (+) = CRENEAU.DEBSEMC
and ENSEIGNER.JOURC (+) = CRENEAU.JOURC
and ENSEIGNER.HEUREDC (+) = CRENEAU.HEUREDC
and ENSEIGNER.GRPC (+) = CRENEAU.GRPC;

-- Ajout des tables manquantes

create table FORMATION as select * from thierry_millan.FORMATION2018;
create table SALLE as select * from thierry_millan.SALLE2018;
create table ENSEIGNANT as select * from thierry_millan.ENSEIGNANT2018;
create table STATUT as select * from thierry_millan.STATUT2018;

commit;

-- Ajout des contraintes de clefs primaires

alter table FORMATION add constraint PK_FORMATION primary key(IdFor);
alter table SALLE add constraint PK_SALLE primary key(NSalle);
alter table ENSEIGNANT add constraint PK_ENSEIGNANT primary key(IdEnseign);
alter table STATUT add constraint PK_STATUT primary key(Grade);

alter table GROUPE add constraint FK_GROUPE_ANNEEC foreign key(ANNEEC) references FORMATION(IdFor);
alter table ENSEIGNANT add constraint FK_ENSEIGNANT_GRADE foreign key(Grade) references STATUT(Grade);
alter table ENSEIGNER add constraint FK_ENSEIGNER_ENSC foreign key(ENSC) references ENSEIGNANT(IdEnseign);
alter table AFFECTER add constraint FK_AFFECTER_SALC foreign key(SALC) references SALLE(NSalle);

alter table GROUPE add Eff number(3) default 0;

update GROUPE set Eff = (select Eff from thierry_millan.EFFORMATION2018 where IDGRP = GROUPE.GRPC);

-- Requêtes

select * from CRENEAU;

-- 4.a

select GROUPE.GRPC, GROUPE.Eff
from CRENEAU, GROUPE 
where CRENEAU.GRPC = GROUPE.GRPC 
and DEBSEMC = '19-SEP-16'
and JOURC = 'mercredi'
and HEUREDC = '11:00'
and MATC = 'InM1101';

select * from creneau;

-- 4.b

select MAT.MATC, sum((to_date(HEUREFC, 'HH24:MI')-to_date(HEUREDC, 'HH24:MI'))*24)
from CRENEAU, MAT
where CRENEAU.MATC = MAT.MATC
and CRENEAU.MATC is not null
group by MAT.MATC
order by 1;

-- 4.c
create or replace function dateOffset(d1 varchar2, d2 varchar2) return float as
begin
    return (to_date(d1, 'HH24:MI') - to_date(d2, 'HH24:MI')) * 24.0;
end;

create or replace view NbHdispenseParEnseignant as
select ENSEIGNANT.idenseign, ENSEIGNANT.prenom, ENSEIGNANT.nom, 
sum((case CRENEAU.TYPEC
when 'CM' then dateOffset(CRENEAU.HEUREFC, CRENEAU.HEUREDC)*1.5
else dateOffset(CRENEAU.HEUREFC, CRENEAU.HEUREDC)
end)) nbHeuresDispensees
from ENSEIGNER, CRENEAU, ENSEIGNANT
where ENSEIGNANT.IdEnseign = ENSEIGNER.ENSC 
and ENSEIGNER.DEBSEMC = CRENEAU.DEBSEMC
and ENSEIGNER.JOURC = CRENEAU.JOURC
and ENSEIGNER.HEUREDC = CRENEAU.HEUREDC
and ENSEIGNER.GRPC = CRENEAU.GRPC
and CRENEAU.MATC is not null
group by ENSEIGNANT.idenseign, ENSEIGNER.ENSC, ENSEIGNANT.prenom, ENSEIGNANT.nom;

-- 4.d

update enseignant
set nbhdisp = (
select nbheuresdispensees 
from nbhdispenseparenseignant n
where enseignant.idenseign = idenseign
);

select * from creneau;

-- 5.a

select * from affecter;

drop trigger t_b_i_affecter;

select * from user_triggers where table_name = 'AFFECTER';

create or replace trigger t_b_i_affecter
after insert on affecter
for each row
declare
type_cours creneau.typec%type;
type_salle salle.tsal%type;
begin
    select c.typec, s.tsal into type_cours, type_salle
    from salle s, creneau c
    where c.debsemc = :NEW.debsemc
    and c.jourc = :NEW.jourc
    and c.heuredc = :NEW.heuredc
    and c.grpc = :NEW.grpc
    and s.nsalle = :NEW.salc;
    
    if type_cours <> type_salle then
        raise_application_error(-20001, 'La salle ne convient pas pour ce type d''enseignement');
    end if;
end;

-- Tests

insert into affecter values('05-JUN-17', 'mardi', '08:00', 'InS2F2', 'IN209');

insert into affecter values('05-JUN-17', 'mardi', '08:00', 'InS2F2', 'InJade');

rollback;

insert into affecter values('05-JUN-17', 'mardi', '08:00', 'InSxx', 'In209');

insert into affecter values('05-JUN-17', 'mardi', '08:00', 'InS2F2', 'Inxxx');


-- 5.b

drop trigger t_b_i_affecter;

create or replace trigger t_b_i_affecter
after update on affecter
for each row
declare
type_cours creneau.typec%type;
type_salle salle.tsal%type;
begin
    select c.typec, s.tsal into type_cours, type_salle
    from salle s, creneau c
    where c.debsemc = :NEW.debsemc
    and c.jourc = :NEW.jourc
    and c.heuredc = :NEW.heuredc
    and c.grpc = :NEW.grpc
    and s.nsalle = :NEW.salc;
    
    if type_cours <> type_salle then
        raise_application_error(-20001, 'La salle ne convient pas pour ce type d''enseignement');
    end if;
end;

-- Tests

update affecter
set salc = 'IN209'
where affecter.debsemc = '05-JUN-17'
and affecter.jourc = 'vendredi'
and affecter.heuredc = '08:00'
and affecter.grpc = 'InS2A1'
and affecter.salc = 'InRubis';

update affecter
set salc = 'InJade'
where affecter.debsemc = '05-JUN-17'
and affecter.jourc = 'vendredi'
and affecter.heuredc = '08:00'
and affecter.grpc = 'InS2A1'
and affecter.salc = 'InRubis';

rollback;

-- 5.c

create or replace trigger t_b_iu_affecter_cap
after insert or update on affecter
for each row 
declare
    effectif groupe.eff%type;
    capacite salle.capacite%type;
begin
    select effectif, capacite into effectif, capacite
    from groupe g, salle s
    where g.grpc = :NEW.grpc
    and s.nsalle = :NEW.salc;
    if effectif > capacite then
      raise_application_error(-20002, 'La salle ne convient pas autant d''élèves');
    end if;
end;

insert into creneau values('17-JUL-17', 'mercredi', '17:00', 'CM', '18:30', 'InS2', 'InM2203');

insert into affecter(debsemc, jourc, heuredc, grpc, salc) values('17-JUL-17', 'mercredi', '17:00', 'InS2', 'IR028');

rollback;

insert into creneau values('17-JUL-17', 'mercredi', '17:00', 'CM', '18:30', 'InS2', 'InM2203');

insert into affecter (debsemc, jourc, heuredc, grpc, salc) values('17-JUL-17', 'mercredi', '17:00', 'InS2', 'In203');

rollback;

insert into creneau values('17-JUL-17', 'mercredi', '17:00', 'CM', '18:30', 'InS2', 'InM2203');

insert into affecter(debsemc, jourc, heuredc, grpc, salc) values ('17-JUL-17', 'mercredi', '17:00', 'InS2', 'IR028');

update affecter set salc = 'InJade'
where affecter.debsemc = '24-APR-17'
and affecter.jourc = 'lundi'
and affecter.heuredc = '15:30'
and affecter.grpc = 'InS2'
and affecter.salc = 'IR028';

rollback;


-- 5.d

create or replace trigger t_b_i_creneau
before insert on creneau
for each row 
declare
    counts int;
begin
    select count(*) into counts
    from creneau
    where debsemc = :NEW.debsemc
    and jourc = :NEW.jourc
    and grpc = :NEW.grpc
    and (
        to_date(:NEW.heuredc, 'HH24:MI') between to_date(heuredc, 'HH24:MI') and to_date(heurefc, 'HH24:MI')
        or to_date(:NEW.heurefc, 'HH24:MI') between to_date(heuredc, 'HH24:MI') and to_date(heurefc, 'HH24:MI')
        or (to_date(:new.heuredc,  'HH24:MI') < to_date(heuredc, 'HH24:MI') and to_date(:new.heurefc, 'HH24:MI') > to_date(heurefc, 'HH24:MI'))
    );
    
    if counts <> 0 then
        raise_application_error(-20003, 'Un groupe ne peut pas avoir deux enseignements en même temps');
    end if;
end;

insert into creneau values('05-JUN-17', 'mardi', '08:05', 'TP', '09:30', 'InS2F2', 'InM2104');

insert into creneau values('05-JUN-17', 'mardi', '07:45', 'TP', '13:00', 'InS2F2', 'InM2104');

rollback;

-- 6. 

select * from enseigner;

select * from creneau;

select * from enseignant;

select * from nbhdispenseparenseignant;

set serveroutput on;

create or replace trigger t_b_i_enseigner
before insert or update or delete on Enseigner
for each row
declare
typeCours creneau.typec%type;
heureFin creneau.heurefc%type;
toAdd float;
begin
    select typec, heurefc into typeCours, heureFin
    from creneau
    where debsemc = :NEW.debsemc
    and jourc = :NEW.jourc
    and heuredc = :NEW.heuredc
    and grpc = :NEW.grpc;
    
    if typeCours in ('CM', 'TD', 'TP') then    
        toAdd := (to_date(heureFin, 'HH24:MI') - to_date(:NEW.heuredc, 'HH24:MI')) * 24;
        if typeCours = 'CM' then
            toAdd := toAdd * 1.5;
        end if;   
        
        if DELETING then
            toAdd := -toAdd;
        end if;
            
        update Enseignant
        set nbhdisp = nbhdisp + toAdd
        where idenseign = :NEW.ensc;
    end if;
end;

select * from enseignant where idenseign = 'DBJ';

insert into enseigner values('06-FEB-17', 'mardi', '15:30', 'InS4C', 'DBJ');

select * from enseignant where idenseign = 'DBJ';

rollback;

select * from enseigner 
where debsemc = '06-FEB-17'
and jourc = 'mardi'
and heuredc = '15:30'
and grpc = 'InS4C';

delete from enseigner 
where debsemc = '06-FEB-17'
and jourc = 'mardi'
and heuredc = '15:30'
and grpc = 'InS4C'
and ensc = 'DBJ';




