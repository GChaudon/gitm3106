-- 1. Index

select * from USER_INDEXES;

select * from USER_IND_COLUMNS;

-- a. Les index automatiquement créés par Oracle sont ceux des clés étrangères

-- b. Il est souhaitable de créer des indes pour les attributs utilisés comme critère de jointure (clés étrangères)
-- Sur les attributs servant souvent de critères de sélection et sur une table de gros volume

-- c.
-- GRPC ==> Creneau

select * from creneau;

create index IdxGrpcCreneau on creneau(grpc);


-- NOM ==> Enseignant

select * from enseignant;

create index IdxNomEnseignant on enseignant(nom);

-- ENSC ==> Enseigner

select * from enseigner;

create index IdxEnscEnseigner on enseigner(ensc);

-- D. Le type bitmap semble le plus approprié

select * from creneau;

create bitmap index IdXTypeCCreneau on creneau(typec);

-- e.

drop index IdxGrpcCreneau;
drop index IdxNomEnseignant;
drop index IdXEnscEnseigner;
drop index IdXTypeCCreneau;

-- 2. Plan d'exécution

select * from creneau;

set autotrace on;

select * from creneau
where matc = 'inA1108';

-- 3.

set autotrace on;

select nom 
from enseignant
where idenseign = 'DBJ';


-- 4.

set autotrace on;

select * from enseignant where nom = 'DU BELLAY';

drop index IdxNomEnseignant;

create index IdXNomEnseignant on enseignant(nom);

select * from enseignant where nom = 'DU BELLAY';

call dbms_stats.gather_schema_stats(user, 100);


-- 5.

select * from enseignant, enseigner
where enseigner.ensc = enseignant.idenseign;

drop index IdxEnscEnseigner;

create index IdxEnscEnseigner on enseigner(ensc);

-- La requête sans l'index appliquée est la plus optimisée, car elle dispose du même coût qu'avec un index
-- Et l'index va ralentir la db lors de l'insertion de valeurs + prendre de l'espace

-- 6.

-- a.

select c.matc, e.ensc, sum((to_date(heurefc, 'HH24:MI') - to_date(c.heuredc, 'HH24:MI'))*24)
from creneau c, enseigner e
where c.debsemc = e.debsemc
and c.jourc = e.jourc
and c.heuredc = e.heuredc
and c.grpc = e.grpc
group by c.matc, e.ensc
having sum((to_date(heurefc, 'HH24:MI') - to_date(c.heuredc, 'HH24:MI'))*24) > 5;

-- b.

select c.matc, e.ensc, count(*)
from creneau c, enseigner e
where c.debsemc = e.debsemc
and c.jourc = e.jourc
and c.heuredc = e.heuredc
and c.grpc = e.grpc
group by c.matc, e.ensc;

-- Il y a une différence de cardinalité entre les deux plans d'exécution

-- 7. 

select * from enseigner
where debsemc = '31-OCT-16'
and jourc = 'vendredi'
and heuredc = '08:00'
and grpc = 'InS1A'
and ensc = 'FUA';


delete from enseigner
where debsemc = '31-OCT-16'
and jourc = 'vendredi'
and heuredc = '08:00'
and grpc = 'InS1A'
and ensc = 'FUA';

insert into enseigner values('31-OCT-16', 'vendredi', '08:00', 'InS1A', 'FUA');

create or replace view nbhdispenseparenseignant as
(select ensc, sum(1.5 * (to_date(heurefc, 'HH24:MI') - to_date(e.heuredc, 'HH24:MI'))*24) nbh
from creneau c, enseigner e
where typec = 'CM'
and e.debsemc = c.debsemc
and e.jourc = c.jourc
and e.heuredc = c.heuredc
and e.grpc = c.grpc
group by ensc)
union all
(select ensc, sum((to_date(heurefc, 'HH24:MI') - to_date(e.heuredc, 'HH24:MI'))*24) nbh
from creneau c, enseigner e
where typec in ('TD', 'TP')
and e.debsemc = c.debsemc
and e.jourc = c.jourc
and e.heuredc = c.heuredc
and e.grpc = c.grpc
group by ensc);

update enseignant
set nbhdisp = (
    select sum(nbh) 
    from nbhdispenseparenseignant 
    where ensc = idenseign
);
