-- Exercice 4.

-- 1. Les actions du DDL (modification de la structure de la DB)

create table T (A number constraint PK_A primary key, B varchar2(5));

drop table T;

alter table T add C varchar2(5);

alter table T drop column C;

-- 2. Les actions du DML (modification des données)

insert into t values(1, 'a');

select * from T;

-- La transaction n'est pas validée et n'est donc pas visible depuis l'autre session

commit;

select * from T;

delete from T;

commit;

-- 3. 

insert into T values(1, 'a');
insert into T values(2, 'b');
insert into T values(3, 'c');
insert into T values(4, 'd');
delete from T where b = 'b';
select * from t;

rollback;

select * from t;

-- Le rollback annule les opérations de la transaction

-- 4. 

insert into T values(2, 'b');
insert into T values(3, 'e');
insert into T values(5, 'f');
insert into T values(6, 'g');
savepoint point_de_reprise;
delete from T where b = 'b';
select * from t;

rollback to savepoint point_de_reprise;
select * from t;

commit;
select * from t;

-- 5.

update t set b = 'K' where a = 2;
select * from T;
commit;

update T set b = 'Z' where a = 2;

commit;



update t set b = 'A1' where a = 2;
update t set b = 'A2' where a = 3;

-- deadlock detected while waiting for ressource