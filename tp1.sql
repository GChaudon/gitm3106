drop table MAT;

create table MAT(
matc varchar2(20),
intc varchar(100),
constraint pk_mat primary key(matc)
);

alter table MAT modify intc varchar2(100);

select * from mat;

delete from MAT;

insert into MAT(matc, intc)
select distinct matc, intc 
from thierry_millan.CELCAT2017
where matc is not null;

-- Ou 


create table MAT as
select distinct matc, intc
from thierry_millan.CELCAT2017
where matc is not null;

alter table MAT add constraint pk_mat primary key(matc);

select * from mat;

commit;