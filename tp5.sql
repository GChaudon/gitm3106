select * from creneau where heuredc = '11:00';

select * from enseigner;
select * from affecter

rollback;

select * from enseignant where idenseign = 'DRE';

create procedure validerHeures(pIdEnseign in enseignant.idenseign%type, pNbh in enseignant.nbhdisp%type) as
begin
    if pNbh <= 0 then
        raise_application_error(-20001, 'Le nombre d''heures doit être positif');
    else
        update enseignant set nbhdisp = nbhdisp + pNbh where idenseign = pIdEnseign;
    end if;
end;

call validerHeures('DRE', 1.5);

commit;

select * from enseignant order by idenseign;

select * from enseignant where idenseign = 'DRE';

create or replace function nbHDispTotal return float as
total float;
begin
    select sum(nbhdisp) into total from enseignant;
    return total;
end;

select nbHDispTotal() from dual;

create function listeEnseignants return sys_refcursor as 
    curEns sys_refcursor;
begin
    open curEns for 
    select nom, grade from enseignant, enseigner where ensc = idenseign group by idenseign, nom, grade having sum(nbhdisp) > 0;
    return curEns;
end;

create table foo(
    a integer
);

select * from foo;